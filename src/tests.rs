// solve-sudoku9x9 tests

use super::*;

fn get_problem() -> Vec<Vec<u32>> {
    // 出典
    //  https://en.wikipedia.org/wiki/File:Sudoku_Puzzle_by_L2G-20050714_standardized_layout.svg
    //
    //     0: 1: 2:  3: 4: 5:  6: 7: 8:
    //
    // 0:  5, 3, 0,  0, 7, 0,  0, 0, 0
    // 1:  6, 0, 0,  1, 9, 5,  0, 0, 0
    // 2:  0, 9, 8,  0, 0, 0,  0, 6, 0
    //
    // 3:  8, 0, 0,  0, 6, 0,  0, 0, 3
    // 4:  4, 0, 0,  8, 0, 3,  0, 0, 1
    // 5:  7, 0, 0,  0, 2, 0,  0, 0, 6
    //
    // 6:  0, 6, 0,  0, 0, 0,  2, 8, 0
    // 7:  0, 0, 0,  4, 1, 9,  0, 0, 5
    // 8:  0, 0, 0,  0, 8, 0,  0, 7, 9
    //
    vec![
        vec![5, 3, 0, 0, 7, 0, 0, 0, 0],
        vec![6, 0, 0, 1, 9, 5, 0, 0, 0],
        vec![0, 9, 8, 0, 0, 0, 0, 6, 0],
        vec![8, 0, 0, 0, 6, 0, 0, 0, 3],
        vec![4, 0, 0, 8, 0, 3, 0, 0, 1],
        vec![7, 0, 0, 0, 2, 0, 0, 0, 6],
        vec![0, 6, 0, 0, 0, 0, 2, 8, 0],
        vec![0, 0, 0, 4, 1, 9, 0, 0, 5],
        vec![0, 0, 0, 0, 8, 0, 0, 7, 9],
    ]
}

#[test]
fn test_solve() {
    // せめてデフォルトでは例題を解けるように…したい
    {
        let solver = Solver::default();
        let mut field = get_problem();
        let result = solver.solve(&mut field);
        assert!(result.is_ok(), "field: {:?}, result: {:?}", field, result);

        assert_eq!(
            field,
            [
                [5, 3, 4, 6, 7, 8, 9, 1, 2],
                [6, 7, 2, 1, 9, 5, 3, 4, 8],
                [1, 9, 8, 3, 4, 2, 5, 6, 7],
                [8, 5, 9, 7, 6, 1, 4, 2, 3],
                [4, 2, 6, 8, 5, 3, 7, 9, 1],
                [7, 1, 3, 9, 2, 4, 8, 5, 6],
                [9, 6, 1, 5, 3, 7, 2, 8, 4],
                [2, 8, 7, 4, 1, 9, 6, 3, 5],
                [3, 4, 5, 2, 8, 6, 1, 7, 9],
            ],
            "field {:?}",
            field
        );
    }

    // テキトーなパラメータでも解けるといいな・・・
    {
        let solver = Solver::new(50, 3000, 0.9, 1_2345_6789).unwrap();
        let mut field = get_problem();
        let result = solver.solve(&mut field);
        assert!(result.is_ok(), "field: {:?}, result: {:?}", field, result);

        assert_eq!(
            field,
            [
                [5, 3, 4, 6, 7, 8, 9, 1, 2],
                [6, 7, 2, 1, 9, 5, 3, 4, 8],
                [1, 9, 8, 3, 4, 2, 5, 6, 7],
                [8, 5, 9, 7, 6, 1, 4, 2, 3],
                [4, 2, 6, 8, 5, 3, 7, 9, 1],
                [7, 1, 3, 9, 2, 4, 8, 5, 6],
                [9, 6, 1, 5, 3, 7, 2, 8, 4],
                [2, 8, 7, 4, 1, 9, 6, 3, 5],
                [3, 4, 5, 2, 8, 6, 1, 7, 9],
            ],
            "field {:?}",
            field
        );
    }

    // テキトーなパラメータでも解けるといいな・・・
    {
        let solver = Solver::new(20, 8000, 0.5, 9_8765_4321).unwrap();
        let mut field = get_problem();
        let result = solver.solve(&mut field);
        assert!(result.is_ok(), "field: {:?}, result: {:?}", field, result);

        assert_eq!(
            field,
            [
                [5, 3, 4, 6, 7, 8, 9, 1, 2],
                [6, 7, 2, 1, 9, 5, 3, 4, 8],
                [1, 9, 8, 3, 4, 2, 5, 6, 7],
                [8, 5, 9, 7, 6, 1, 4, 2, 3],
                [4, 2, 6, 8, 5, 3, 7, 9, 1],
                [7, 1, 3, 9, 2, 4, 8, 5, 6],
                [9, 6, 1, 5, 3, 7, 2, 8, 4],
                [2, 8, 7, 4, 1, 9, 6, 3, 5],
                [3, 4, 5, 2, 8, 6, 1, 7, 9],
            ],
            "field {:?}",
            field
        );
    }
}

#[test]
fn test_check_duplicate_value_in_rows() {
    {
        let field = get_problem();

        assert_eq!(check_duplicate_value_in_rows(&field), Ok(()));
    }

    {
        let mut field = get_problem();

        field[6][0] = 5;
        field[6][4] = 5;

        assert_eq!(
            check_duplicate_value_in_rows(&field),
            Err(Error::DuplicateValue {
                row: 6,
                col: 4,
                value: 5
            })
        );
    }

    {
        let mut field = vec![vec![0_u32; 9]; 9];

        for u in 0..80_usize {
            let u_row = u / 9;
            let u_col = u % 9;
            for w in u + 1..81_usize {
                let w_row = w / 9;
                let w_col = w % 9;
                for value in 1..=9_u32 {
                    field[u_row][u_col] = value;
                    field[w_row][w_col] = value;
                    let res = check_duplicate_value_in_rows(&field);
                    if u_row == w_row {
                        assert_eq!(
                            res,
                            Err(Error::DuplicateValue {
                                row: w_row,
                                col: w_col,
                                value,
                            })
                        );
                    } else {
                        assert_eq!(res, Ok(()));
                    }
                }
                field[u_row][u_col] = 0;
                field[w_row][w_col] = 0;
            }
        }
    }
}

#[test]
fn test_check_duplicate_value_in_cols() {
    {
        let field = get_problem();

        assert_eq!(check_duplicate_value_in_cols(&field), Ok(()));
    }

    {
        let mut field = get_problem();

        field[1][6] = 5;
        field[5][6] = 5;

        assert_eq!(
            check_duplicate_value_in_cols(&field),
            Err(Error::DuplicateValue {
                row: 5,
                col: 6,
                value: 5
            })
        );
    }

    {
        let mut field = vec![vec![0_u32; 9]; 9];

        for u in 0..80_usize {
            let u_row = u / 9;
            let u_col = u % 9;
            for w in u + 1..81_usize {
                let w_row = w / 9;
                let w_col = w % 9;
                for value in 1..=9_u32 {
                    field[u_row][u_col] = value;
                    field[w_row][w_col] = value;
                    let res = check_duplicate_value_in_cols(&field);
                    if u_col == w_col {
                        assert_eq!(
                            res,
                            Err(Error::DuplicateValue {
                                row: w_row,
                                col: w_col,
                                value,
                            })
                        );
                    } else {
                        assert_eq!(res, Ok(()));
                    }
                }
                field[u_row][u_col] = 0;
                field[w_row][w_col] = 0;
            }
        }
    }
}

#[test]
fn test_check_duplicate_value_in_blocks() {
    {
        let field = get_problem();

        assert_eq!(check_duplicate_value_in_blocks(&field), Ok(()));
    }

    {
        let mut field = get_problem();

        field[6][0] = 5;
        field[8][2] = 5;

        assert_eq!(
            check_duplicate_value_in_blocks(&field),
            Err(Error::DuplicateValue {
                row: 8,
                col: 2,
                value: 5
            })
        );
    }

    {
        let mut field = vec![vec![0_u32; 9]; 9];

        for u in 0..80_usize {
            let u_row = u / 9;
            let u_col = u % 9;
            let u_area_row = u_row / 3;
            let u_area_col = u_col / 3;
            for w in u + 1..81_usize {
                let w_row = w / 9;
                let w_col = w % 9;
                let w_area_row = w_row / 3;
                let w_area_col = w_col / 3;
                for value in 1..=9_u32 {
                    field[u_row][u_col] = value;
                    field[w_row][w_col] = value;
                    let res = check_duplicate_value_in_blocks(&field);
                    if u_area_row == w_area_row && u_area_col == w_area_col {
                        assert_eq!(
                            res,
                            Err(Error::DuplicateValue {
                                row: w_row,
                                col: w_col,
                                value,
                            })
                        );
                    } else {
                        assert_eq!(res, Ok(()));
                    }
                }
                field[u_row][u_col] = 0;
                field[w_row][w_col] = 0;
            }
        }
    }
}

#[test]
fn test_check_wrong_size() {
    {
        let field = get_problem();

        assert_eq!(check_wrong_size(&field), Ok(()));
    }

    {
        let field = vec![vec![0_u32; 9]; 2];

        assert_eq!(check_wrong_size(&field), Err(Error::WrongSize));
    }

    {
        let field = vec![vec![0_u32; 3]; 9];

        assert_eq!(check_wrong_size(&field), Err(Error::WrongSize));
    }
}

#[test]
fn test_check_wrong_value() {
    {
        let field = get_problem();

        assert_eq!(check(&field), Ok(()));
    }

    {
        let mut field = get_problem();

        field[3][3] = 10;

        assert_eq!(
            check_wrong_value(&field),
            Err(Error::WrongValue {
                row: 3,
                col: 3,
                value: 10
            })
        );
    }

    {
        let mut field = vec![vec![0_u32; 9]; 9];
        let value = 10;
        for row in 0..9 {
            for col in 0..9 {
                field[row][col] = value;
                assert_eq!(
                    check_wrong_value(&field),
                    Err(Error::WrongValue { row, col, value })
                );
                field[row][col] = 0;
            }
        }
    }
}

#[test]
fn test_check() {
    {
        let field = get_problem();

        assert_eq!(check(&field), Ok(()));
    }

    {
        let field = vec![vec![0_u32; 9]; 2];

        assert_eq!(check(&field), Err(Error::WrongSize));
    }

    {
        let field = vec![vec![0_u32; 3]; 9];

        assert_eq!(check(&field), Err(Error::WrongSize));
    }

    {
        let mut field = get_problem();

        field[3][3] = 10;

        assert_eq!(
            check(&field),
            Err(Error::WrongValue {
                row: 3,
                col: 3,
                value: 10
            })
        );
    }

    // 同一行
    {
        let mut field = get_problem();

        field[6][2] = 5;
        field[6][3] = 5;

        assert_eq!(
            check(&field),
            Err(Error::DuplicateValue {
                row: 6,
                col: 3,
                value: 5
            })
        );
    }

    // 同一列
    {
        let mut field = get_problem();

        field[2][6] = 5;
        field[3][6] = 5;

        assert_eq!(
            check(&field),
            Err(Error::DuplicateValue {
                row: 3,
                col: 6,
                value: 5
            })
        );
    }

    // 行も列も異なる(同一ブロック内)
    {
        let mut field = get_problem();

        field[3][6] = 5;
        field[5][7] = 5;

        assert_eq!(
            check(&field),
            Err(Error::DuplicateValue {
                row: 5,
                col: 7,
                value: 5
            })
        );
    }

    {
        let mut field = vec![vec![0_u32; 9]; 9];
        let value = 10;
        for row in 0..9 {
            for col in 0..9 {
                field[row][col] = value;
                assert_eq!(check(&field), Err(Error::WrongValue { row, col, value }));
                field[row][col] = 0;
            }
        }
    }

    {
        let mut field = vec![vec![0_u32; 9]; 9];

        for u in 0..80_usize {
            let u_row = u / 9;
            let u_col = u % 9;
            let u_area_row = u_row / 3;
            let u_area_col = u_col / 3;
            for w in u + 1..81_usize {
                let w_row = w / 9;
                let w_col = w % 9;
                let w_area_row = w_row / 3;
                let w_area_col = w_col / 3;
                for value in 1..=9_u32 {
                    field[u_row][u_col] = value;
                    field[w_row][w_col] = value;
                    let res = check(&field);
                    let bad_row = u_row == w_row;
                    let bad_col = u_col == w_col;
                    let bad_area = u_area_row == w_area_row && u_area_col == w_area_col;
                    let bad = bad_row || bad_col || bad_area;
                    if bad {
                        assert_eq!(
                            res,
                            Err(Error::DuplicateValue {
                                row: w_row,
                                col: w_col,
                                value,
                            })
                        );
                    } else {
                        assert_eq!(res, Ok(()));
                    }
                }
                field[u_row][u_col] = 0;
                field[w_row][w_col] = 0;
            }
        }
    }
}

#[test]
fn test_make_pairs() {
    let field = get_problem();

    let pairs = make_pairs(&field);

    assert_eq!(
        pairs[..6],
        [
            (Pos { row: 0, col: 2 }, Pos { row: 1, col: 1 }),
            (Pos { row: 0, col: 2 }, Pos { row: 1, col: 2 }),
            (Pos { row: 0, col: 2 }, Pos { row: 2, col: 0 }),
            (Pos { row: 1, col: 1 }, Pos { row: 1, col: 2 }),
            (Pos { row: 1, col: 1 }, Pos { row: 2, col: 0 }),
            (Pos { row: 1, col: 2 }, Pos { row: 2, col: 0 }),
        ],
        "上段左"
    );

    assert_eq!(
        pairs[6..16],
        [
            (Pos { row: 0, col: 3 }, Pos { row: 0, col: 5 }),
            (Pos { row: 0, col: 3 }, Pos { row: 2, col: 3 }),
            (Pos { row: 0, col: 3 }, Pos { row: 2, col: 4 }),
            (Pos { row: 0, col: 3 }, Pos { row: 2, col: 5 }),
            (Pos { row: 0, col: 5 }, Pos { row: 2, col: 3 }),
            (Pos { row: 0, col: 5 }, Pos { row: 2, col: 4 }),
            (Pos { row: 0, col: 5 }, Pos { row: 2, col: 5 }),
            (Pos { row: 2, col: 3 }, Pos { row: 2, col: 4 }),
            (Pos { row: 2, col: 3 }, Pos { row: 2, col: 5 }),
            (Pos { row: 2, col: 4 }, Pos { row: 2, col: 5 }),
        ],
        "上段中央"
    );

    assert_eq!(
        pairs[16..44],
        [
            (Pos { row: 0, col: 6 }, Pos { row: 0, col: 7 }),
            (Pos { row: 0, col: 6 }, Pos { row: 0, col: 8 }),
            (Pos { row: 0, col: 6 }, Pos { row: 1, col: 6 }),
            (Pos { row: 0, col: 6 }, Pos { row: 1, col: 7 }),
            (Pos { row: 0, col: 6 }, Pos { row: 1, col: 8 }),
            (Pos { row: 0, col: 6 }, Pos { row: 2, col: 6 }),
            (Pos { row: 0, col: 6 }, Pos { row: 2, col: 8 }),
            (Pos { row: 0, col: 7 }, Pos { row: 0, col: 8 }),
            (Pos { row: 0, col: 7 }, Pos { row: 1, col: 6 }),
            (Pos { row: 0, col: 7 }, Pos { row: 1, col: 7 }),
            (Pos { row: 0, col: 7 }, Pos { row: 1, col: 8 }),
            (Pos { row: 0, col: 7 }, Pos { row: 2, col: 6 }),
            (Pos { row: 0, col: 7 }, Pos { row: 2, col: 8 }),
            (Pos { row: 0, col: 8 }, Pos { row: 1, col: 6 }),
            (Pos { row: 0, col: 8 }, Pos { row: 1, col: 7 }),
            (Pos { row: 0, col: 8 }, Pos { row: 1, col: 8 }),
            (Pos { row: 0, col: 8 }, Pos { row: 2, col: 6 }),
            (Pos { row: 0, col: 8 }, Pos { row: 2, col: 8 }),
            (Pos { row: 1, col: 6 }, Pos { row: 1, col: 7 }),
            (Pos { row: 1, col: 6 }, Pos { row: 1, col: 8 }),
            (Pos { row: 1, col: 6 }, Pos { row: 2, col: 6 }),
            (Pos { row: 1, col: 6 }, Pos { row: 2, col: 8 }),
            (Pos { row: 1, col: 7 }, Pos { row: 1, col: 8 }),
            (Pos { row: 1, col: 7 }, Pos { row: 2, col: 6 }),
            (Pos { row: 1, col: 7 }, Pos { row: 2, col: 8 }),
            (Pos { row: 1, col: 8 }, Pos { row: 2, col: 6 }),
            (Pos { row: 1, col: 8 }, Pos { row: 2, col: 8 }),
            (Pos { row: 2, col: 6 }, Pos { row: 2, col: 8 }),
        ],
        "上段右"
    );

    assert_eq!(
        pairs[44..59],
        [
            (Pos { row: 3, col: 1 }, Pos { row: 3, col: 2 }),
            (Pos { row: 3, col: 1 }, Pos { row: 4, col: 1 }),
            (Pos { row: 3, col: 1 }, Pos { row: 4, col: 2 }),
            (Pos { row: 3, col: 1 }, Pos { row: 5, col: 1 }),
            (Pos { row: 3, col: 1 }, Pos { row: 5, col: 2 }),
            (Pos { row: 3, col: 2 }, Pos { row: 4, col: 1 }),
            (Pos { row: 3, col: 2 }, Pos { row: 4, col: 2 }),
            (Pos { row: 3, col: 2 }, Pos { row: 5, col: 1 }),
            (Pos { row: 3, col: 2 }, Pos { row: 5, col: 2 }),
            (Pos { row: 4, col: 1 }, Pos { row: 4, col: 2 }),
            (Pos { row: 4, col: 1 }, Pos { row: 5, col: 1 }),
            (Pos { row: 4, col: 1 }, Pos { row: 5, col: 2 }),
            (Pos { row: 4, col: 2 }, Pos { row: 5, col: 1 }),
            (Pos { row: 4, col: 2 }, Pos { row: 5, col: 2 }),
            (Pos { row: 5, col: 1 }, Pos { row: 5, col: 2 }),
        ],
        "中段左"
    );

    assert_eq!(
        pairs[59..69],
        [
            (Pos { row: 3, col: 3 }, Pos { row: 3, col: 5 }),
            (Pos { row: 3, col: 3 }, Pos { row: 4, col: 4 }),
            (Pos { row: 3, col: 3 }, Pos { row: 5, col: 3 }),
            (Pos { row: 3, col: 3 }, Pos { row: 5, col: 5 }),
            (Pos { row: 3, col: 5 }, Pos { row: 4, col: 4 }),
            (Pos { row: 3, col: 5 }, Pos { row: 5, col: 3 }),
            (Pos { row: 3, col: 5 }, Pos { row: 5, col: 5 }),
            (Pos { row: 4, col: 4 }, Pos { row: 5, col: 3 }),
            (Pos { row: 4, col: 4 }, Pos { row: 5, col: 5 }),
            (Pos { row: 5, col: 3 }, Pos { row: 5, col: 5 }),
        ],
        "中段中央"
    );

    assert_eq!(
        pairs[69..84],
        [
            (Pos { row: 3, col: 6 }, Pos { row: 3, col: 7 }),
            (Pos { row: 3, col: 6 }, Pos { row: 4, col: 6 }),
            (Pos { row: 3, col: 6 }, Pos { row: 4, col: 7 }),
            (Pos { row: 3, col: 6 }, Pos { row: 5, col: 6 }),
            (Pos { row: 3, col: 6 }, Pos { row: 5, col: 7 }),
            (Pos { row: 3, col: 7 }, Pos { row: 4, col: 6 }),
            (Pos { row: 3, col: 7 }, Pos { row: 4, col: 7 }),
            (Pos { row: 3, col: 7 }, Pos { row: 5, col: 6 }),
            (Pos { row: 3, col: 7 }, Pos { row: 5, col: 7 }),
            (Pos { row: 4, col: 6 }, Pos { row: 4, col: 7 }),
            (Pos { row: 4, col: 6 }, Pos { row: 5, col: 6 }),
            (Pos { row: 4, col: 6 }, Pos { row: 5, col: 7 }),
            (Pos { row: 4, col: 7 }, Pos { row: 5, col: 6 }),
            (Pos { row: 4, col: 7 }, Pos { row: 5, col: 7 }),
            (Pos { row: 5, col: 6 }, Pos { row: 5, col: 7 }),
        ],
        "中段右"
    );

    assert_eq!(
        pairs[84..112],
        [
            (Pos { row: 6, col: 0 }, Pos { row: 6, col: 2 }),
            (Pos { row: 6, col: 0 }, Pos { row: 7, col: 0 }),
            (Pos { row: 6, col: 0 }, Pos { row: 7, col: 1 }),
            (Pos { row: 6, col: 0 }, Pos { row: 7, col: 2 }),
            (Pos { row: 6, col: 0 }, Pos { row: 8, col: 0 }),
            (Pos { row: 6, col: 0 }, Pos { row: 8, col: 1 }),
            (Pos { row: 6, col: 0 }, Pos { row: 8, col: 2 }),
            (Pos { row: 6, col: 2 }, Pos { row: 7, col: 0 }),
            (Pos { row: 6, col: 2 }, Pos { row: 7, col: 1 }),
            (Pos { row: 6, col: 2 }, Pos { row: 7, col: 2 }),
            (Pos { row: 6, col: 2 }, Pos { row: 8, col: 0 }),
            (Pos { row: 6, col: 2 }, Pos { row: 8, col: 1 }),
            (Pos { row: 6, col: 2 }, Pos { row: 8, col: 2 }),
            (Pos { row: 7, col: 0 }, Pos { row: 7, col: 1 }),
            (Pos { row: 7, col: 0 }, Pos { row: 7, col: 2 }),
            (Pos { row: 7, col: 0 }, Pos { row: 8, col: 0 }),
            (Pos { row: 7, col: 0 }, Pos { row: 8, col: 1 }),
            (Pos { row: 7, col: 0 }, Pos { row: 8, col: 2 }),
            (Pos { row: 7, col: 1 }, Pos { row: 7, col: 2 }),
            (Pos { row: 7, col: 1 }, Pos { row: 8, col: 0 }),
            (Pos { row: 7, col: 1 }, Pos { row: 8, col: 1 }),
            (Pos { row: 7, col: 1 }, Pos { row: 8, col: 2 }),
            (Pos { row: 7, col: 2 }, Pos { row: 8, col: 0 }),
            (Pos { row: 7, col: 2 }, Pos { row: 8, col: 1 }),
            (Pos { row: 7, col: 2 }, Pos { row: 8, col: 2 }),
            (Pos { row: 8, col: 0 }, Pos { row: 8, col: 1 }),
            (Pos { row: 8, col: 0 }, Pos { row: 8, col: 2 }),
            (Pos { row: 8, col: 1 }, Pos { row: 8, col: 2 }),
        ],
        "下段左"
    );

    assert_eq!(
        pairs[112..122],
        [
            (Pos { row: 6, col: 3 }, Pos { row: 6, col: 4 }),
            (Pos { row: 6, col: 3 }, Pos { row: 6, col: 5 }),
            (Pos { row: 6, col: 3 }, Pos { row: 8, col: 3 }),
            (Pos { row: 6, col: 3 }, Pos { row: 8, col: 5 }),
            (Pos { row: 6, col: 4 }, Pos { row: 6, col: 5 }),
            (Pos { row: 6, col: 4 }, Pos { row: 8, col: 3 }),
            (Pos { row: 6, col: 4 }, Pos { row: 8, col: 5 }),
            (Pos { row: 6, col: 5 }, Pos { row: 8, col: 3 }),
            (Pos { row: 6, col: 5 }, Pos { row: 8, col: 5 }),
            (Pos { row: 8, col: 3 }, Pos { row: 8, col: 5 }),
        ],
        "下段中央"
    );

    assert_eq!(
        pairs[122..],
        [
            (Pos { row: 6, col: 8 }, Pos { row: 7, col: 6 }),
            (Pos { row: 6, col: 8 }, Pos { row: 7, col: 7 }),
            (Pos { row: 6, col: 8 }, Pos { row: 8, col: 6 }),
            (Pos { row: 7, col: 6 }, Pos { row: 7, col: 7 }),
            (Pos { row: 7, col: 6 }, Pos { row: 8, col: 6 }),
            (Pos { row: 7, col: 7 }, Pos { row: 8, col: 6 }),
        ],
        "下段右"
    );
}

#[test]
fn test_fill_blocks() {
    let mut field = get_problem();

    fill_blocks(&mut field);

    //     0: 1: 2:  3: 4: 5:  6: 7: 8:
    //
    // 0:  5, 3, 7,  8, 7, 6,  9, 8, 7
    // 1:  6, 4, 2,  1, 9, 5,  5, 4, 3
    // 2:  1, 9, 8,  4, 3, 2,  2, 6, 1
    //
    // 3:  8, 9, 6,  9, 6, 7,  9, 8, 3
    // 4:  4, 5, 3,  8, 5, 3,  7, 5, 1
    // 5:  7, 2, 1,  4, 2, 1,  4, 2, 6
    //
    // 6:  9, 6, 8,  7, 6, 5,  2, 8, 6
    // 7:  7, 5, 4,  4, 1, 9,  4, 3, 5
    // 8:  3, 2, 1,  3, 8, 2,  1, 7, 9

    assert_eq!(
        field[..3].iter().map(|row| &row[..3]).collect::<Vec<_>>(),
        [[5, 3, 7], [6, 4, 2], [1, 9, 8]],
        "上段左"
    );

    assert_eq!(
        field[..3].iter().map(|row| &row[3..6]).collect::<Vec<_>>(),
        [[8, 7, 6], [1, 9, 5], [4, 3, 2]],
        "上段中央"
    );

    assert_eq!(
        field[..3].iter().map(|row| &row[6..]).collect::<Vec<_>>(),
        [[9, 8, 7], [5, 4, 3], [2, 6, 1]],
        "上段右"
    );

    assert_eq!(
        field[3..6].iter().map(|row| &row[..3]).collect::<Vec<_>>(),
        [[8, 9, 6], [4, 5, 3], [7, 2, 1]],
        "中段左"
    );

    assert_eq!(
        field[3..6].iter().map(|row| &row[3..6]).collect::<Vec<_>>(),
        [[9, 6, 7], [8, 5, 3], [4, 2, 1]],
        "中段中央"
    );

    assert_eq!(
        field[3..6].iter().map(|row| &row[6..]).collect::<Vec<_>>(),
        [[9, 8, 3], [7, 5, 1], [4, 2, 6]],
        "中段右"
    );

    assert_eq!(
        field[6..].iter().map(|row| &row[..3]).collect::<Vec<_>>(),
        [[9, 6, 8], [7, 5, 4], [3, 2, 1]],
        "下段左"
    );

    assert_eq!(
        field[6..].iter().map(|row| &row[3..6]).collect::<Vec<_>>(),
        [[7, 6, 5], [4, 1, 9], [3, 8, 2]],
        "下段中央"
    );

    assert_eq!(
        field[6..].iter().map(|row| &row[6..]).collect::<Vec<_>>(),
        [[2, 8, 6], [4, 3, 5], [1, 7, 9]],
        "下段右"
    );
}

#[test]
fn test_status_new() {
    let mut field = get_problem();
    fill_blocks(&mut field);

    //     0: 1: 2:  3: 4: 5:  6: 7: 8:
    //
    // 0:  5, 3, 7,  8, 7, 6,  9, 8, 7
    // 1:  6, 4, 2,  1, 9, 5,  5, 4, 3
    // 2:  1, 9, 8,  4, 3, 2,  2, 6, 1
    //
    // 3:  8, 9, 6,  9, 6, 7,  9, 8, 3
    // 4:  4, 5, 3,  8, 5, 3,  7, 5, 1
    // 5:  7, 2, 1,  4, 2, 1,  4, 2, 6
    //
    // 6:  9, 6, 8,  7, 6, 5,  2, 8, 6
    // 7:  7, 5, 4,  4, 1, 9,  4, 3, 5
    // 8:  3, 2, 1,  3, 8, 2,  1, 7, 9

    let status = Status::new(&field);

    // rows_state
    //     0:  1: 2: 3:  4: 5: 6:  7: 8: 9:
    //
    // 0: -1, -1,-1, 0, -1, 0, 0,  2, 1, 0  ..  6   6
    // 1: -1,  0, 0, 0,  1, 1, 0, -1,-1, 0  ..  4  10
    // 2: -1,  1, 1, 0,  0,-1, 0, -1, 0, 0  ..  4  14
    //
    // 3: -1, -1,-1, 0, -1,-1, 1,  0, 1, 2  ..  8  22
    // 4: -1,  0,-1, 1,  0, 2,-1,  0, 0,-1  ..  6  28
    // 5: -1,  1, 2,-1,  1,-1, 0,  0,-1,-1  ..  8  36
    //
    // 6: -1, -1, 0,-1, -1, 0, 2,  0, 1, 0  ..  6  42
    // 7: -1,  0,-1, 0,  2, 1,-1,  0,-1, 0  ..  6  48
    // 8: -1,  1, 1, 1, -1,-1,-1,  0, 0, 0  ..  6  54

    assert_eq!(
        status.rows_state,
        [
            [-1, -1, -1, 0, -1, 0, 0, 2, 1, 0],
            [-1, 0, 0, 0, 1, 1, 0, -1, -1, 0],
            [-1, 1, 1, 0, 0, -1, 0, -1, 0, 0],
            [-1, -1, -1, 0, -1, -1, 1, 0, 1, 2],
            [-1, 0, -1, 1, 0, 2, -1, 0, 0, -1],
            [-1, 1, 2, -1, 1, -1, 0, 0, -1, -1],
            [-1, -1, 0, -1, -1, 0, 2, 0, 1, 0],
            [-1, 0, -1, 0, 2, 1, -1, 0, -1, 0],
            [-1, 1, 1, 1, -1, -1, -1, 0, 0, 0],
        ],
        "rows_state"
    );

    // cols_state
    //     0:  1: 2: 3:  4: 5: 6:  7: 8: 9:
    //
    // 0: -1,  0,-1, 0,  0, 0, 0,  1, 0, 0  ..  2   2
    // 1: -1, -1, 1, 0,  0, 1, 0, -1,-1, 1  ..  6   8
    // 2: -1,  1, 0, 0,  0,-1, 0,  0, 1,-1  ..  4  12
    //
    // 3: -1,  0,-1, 0,  2,-1,-1,  0, 1, 0  ..  6  18
    // 4: -1,  0, 0, 0, -1, 0, 1,  0, 0, 0  ..  2  20
    // 5: -1,  0, 1, 0, -1, 1, 0,  0,-1, 0  ..  4  24
    //
    // 6: -1,  0, 1,-1,  1, 0,-1,  0,-1, 1  ..  6  30
    // 7: -1, -1, 0, 0,  0, 0, 0,  0, 2,-1  ..  4  34
    // 8: -1,  1,-1, 1, -1, 0, 1,  0,-1, 0  ..  6  40

    assert_eq!(
        status.cols_state,
        [
            [-1, 0, -1, 0, 0, 0, 0, 1, 0, 0],
            [-1, -1, 1, 0, 0, 1, 0, -1, -1, 1],
            [-1, 1, 0, 0, 0, -1, 0, 0, 1, -1],
            [-1, 0, -1, 0, 2, -1, -1, 0, 1, 0],
            [-1, 0, 0, 0, -1, 0, 1, 0, 0, 0],
            [-1, 0, 1, 0, -1, 1, 0, 0, -1, 0],
            [-1, 0, 1, -1, 1, 0, -1, 0, -1, 1],
            [-1, -1, 0, 0, 0, 0, 0, 0, 2, -1],
            [-1, 1, -1, 1, -1, 0, 1, 0, -1, 0],
        ],
        "cols_state"
    );

    assert_eq!(status.score, 94, "score");
}

#[test]
fn test_status_calc() {
    let mut field = get_problem();

    fill_blocks(&mut field);

    //     0: 1: 2:  3: 4: 5:  6: 7: 8:
    //
    // 0:  5, 3, 7,  8, 7, 6,  9, 8, 7
    // 1:  6, 4, 2,  1, 9, 5,  5, 4, 3
    // 2:  1, 9, 8,  4, 3, 2,  2, 6, 1
    //
    // 3:  8, 9, 6,  9, 6, 7,  9, 8, 3
    // 4:  4, 5, 3,  8, 5, 3,  7, 5, 1
    // 5:  7, 2, 1,  4, 2, 1,  4, 2, 6
    //
    // 6:  9, 6, 8,  7, 6, 5,  2, 8, 6
    // 7:  7, 5, 4,  4, 1, 9,  4, 3, 5
    // 8:  3, 2, 1,  3, 8, 2,  1, 7, 9

    let status = Status::new(&field);

    // rows_state
    //     0:  1: 2: 3:  4: 5: 6:  7: 8: 9:
    //
    // 0: -1, -1,-1, 0, -1, 0, 0,  2, 1, 0  ..  6   6
    // 1: -1,  0, 0, 0,  1, 1, 0, -1,-1, 0  ..  4  10
    // 2: -1,  1, 1, 0,  0,-1, 0, -1, 0, 0  ..  4  14
    //
    // 3: -1, -1,-1, 0, -1,-1, 1,  0, 1, 2  ..  8  22
    // 4: -1,  0,-1, 1,  0, 2,-1,  0, 0,-1  ..  6  28
    // 5: -1,  1, 2,-1,  1,-1, 0,  0,-1,-1  ..  8  36
    //
    // 6: -1, -1, 0,-1, -1, 0, 2,  0, 1, 0  ..  6  42
    // 7: -1,  0,-1, 0,  2, 1,-1,  0,-1, 0  ..  6  48
    // 8: -1,  1, 1, 1, -1,-1,-1,  0, 0, 0  ..  6  54

    // cols_state
    //     0:  1: 2: 3:  4: 5: 6:  7: 8: 9:
    //
    // 0: -1,  0,-1, 0,  0, 0, 0,  1, 0, 0  ..  2   2
    // 1: -1, -1, 1, 0,  0, 1, 0, -1,-1, 1  ..  6   8
    // 2: -1,  1, 0, 0,  0,-1, 0,  0, 1,-1  ..  4  12
    //
    // 3: -1,  0,-1, 0,  2,-1,-1,  0, 1, 0  ..  6  18
    // 4: -1,  0, 0, 0, -1, 0, 1,  0, 0, 0  ..  2  20
    // 5: -1,  0, 1, 0, -1, 1, 0,  0,-1, 0  ..  4  24
    //
    // 6: -1,  0, 1,-1,  1, 0,-1,  0,-1, 1  ..  6  30
    // 7: -1, -1, 0, 0,  0, 0, 0,  0, 2,-1  ..  4  34
    // 8: -1,  1,-1, 1, -1, 0, 1,  0,-1, 0  ..  6  40

    // 同一列
    let p1 = Pos { row: 0, col: 0 }; // 5
    let p2 = Pos { row: 2, col: 0 }; // 1

    // rows_state
    //     0:  1: 2: 3:  4: 5: 6:  7: 8: 9:
    // 0: -1, -1,-1, 0, -1, 0, 0,  2, 1, 0  ..  6
    // 2: -1,  1, 1, 0,  0,-1, 0, -1, 0, 0  ..  4
    // >>>
    // 0: -1,  0,-1, 0, -1,-1, 0,  2, 1, 0  ..  6
    // 2: -1,  0, 1, 0,  0, 0, 0, -1, 0, 0  ..  2

    assert_eq!(
        status.calc(&field, &p1, &p2),
        -2,
        "calc(_,{:?}({}),{:?}({}))",
        p1,
        p1.get(&field),
        p2,
        p2.get(&field)
    );

    // 同一行
    let p1 = Pos { row: 5, col: 3 }; // 4
    let p2 = Pos { row: 5, col: 4 }; // 2

    // cols_state
    //     0:  1: 2: 3:  4: 5: 6:  7: 8: 9:
    // 3: -1,  0,-1, 0,  2,-1,-1,  0, 1, 0  ..  6
    // 4: -1,  0, 0, 0, -1, 0, 1,  0, 0, 0  ..  2
    // >>>
    // 3: -1,  0, 0, 0,  1,-1,-1,  0, 1, 0  ..  4
    // 4: -1,  0,-1, 0,  0, 0, 1,  0, 0, 0  ..  2

    assert_eq!(
        status.calc(&field, &p1, &p2),
        -2,
        "calc(_,{:?}({}),{:?}({}))",
        p1,
        p1.get(&field),
        p2,
        p2.get(&field)
    );

    // 行も列も異なる(同一ブロック内)
    let p1 = Pos { row: 7, col: 3 }; // 4
    let p2 = Pos { row: 6, col: 4 }; // 6

    // rows_state
    //     0:  1: 2: 3:  4: 5: 6:  7: 8: 9:
    // 6: -1, -1, 0,-1, -1, 0, 2,  0, 1, 0  ..  6
    // 7: -1,  0,-1, 0,  2, 1,-1,  0,-1, 0  ..  6
    // >>>
    // 6: -1, -1, 0,-1,  0, 0, 1,  0, 1, 0  ..  4
    // 7: -1,  0,-1, 0,  1, 1, 0,  0,-1, 0  ..  4

    // cols_state
    //     0:  1: 2: 3:  4: 5: 6:  7: 8: 9:
    // 3: -1,  0,-1, 0,  2,-1,-1,  0, 1, 0  ..  6
    // 4: -1,  0, 0, 0, -1, 0, 1,  0, 0, 0  ..  2
    // >>>
    // 3: -1,  0,-1, 0,  1,-1, 0,  0, 1, 0  ..  4
    // 4: -1,  0, 0, 0,  0, 0, 0,  0, 0, 0  ..  0

    assert_eq!(
        status.calc(&field, &p1, &p2),
        -8,
        "calc(_,{:?}({}),{:?}({}))",
        p1,
        p1.get(&field),
        p2,
        p2.get(&field)
    );
}

#[test]
fn test_status_swap() {
    // 同一列
    {
        let mut field = get_problem();

        fill_blocks(&mut field);

        let mut status = Status::new(&field);

        // 同一列
        let p1 = Pos { row: 0, col: 0 }; // 5
        let p2 = Pos { row: 2, col: 0 }; // 1

        status.swap(&mut field, &p1, &p2);

        assert_eq!(
            field,
            [
                [1, 3, 7, 8, 7, 6, 9, 8, 7],
                [6, 4, 2, 1, 9, 5, 5, 4, 3],
                [5, 9, 8, 4, 3, 2, 2, 6, 1],
                [8, 9, 6, 9, 6, 7, 9, 8, 3],
                [4, 5, 3, 8, 5, 3, 7, 5, 1],
                [7, 2, 1, 4, 2, 1, 4, 2, 6],
                [9, 6, 8, 7, 6, 5, 2, 8, 6],
                [7, 5, 4, 4, 1, 9, 4, 3, 5],
                [3, 2, 1, 3, 8, 2, 1, 7, 9],
            ],
            "同一列 swap({:?},{:?}) ... field",
            p1,
            p2
        );

        assert_eq!(
            status.rows_state,
            [
                [-1, 0, -1, 0, -1, -1, 0, 2, 1, 0],
                [-1, 0, 0, 0, 1, 1, 0, -1, -1, 0],
                [-1, 0, 1, 0, 0, 0, 0, -1, 0, 0],
                [-1, -1, -1, 0, -1, -1, 1, 0, 1, 2],
                [-1, 0, -1, 1, 0, 2, -1, 0, 0, -1],
                [-1, 1, 2, -1, 1, -1, 0, 0, -1, -1],
                [-1, -1, 0, -1, -1, 0, 2, 0, 1, 0],
                [-1, 0, -1, 0, 2, 1, -1, 0, -1, 0],
                [-1, 1, 1, 1, -1, -1, -1, 0, 0, 0],
            ],
            "同一列 swap({:?},{:?}) ... rows_state",
            p1,
            p2
        );

        assert_eq!(
            status.cols_state,
            [
                [-1, 0, -1, 0, 0, 0, 0, 1, 0, 0],
                [-1, -1, 1, 0, 0, 1, 0, -1, -1, 1],
                [-1, 1, 0, 0, 0, -1, 0, 0, 1, -1],
                [-1, 0, -1, 0, 2, -1, -1, 0, 1, 0],
                [-1, 0, 0, 0, -1, 0, 1, 0, 0, 0],
                [-1, 0, 1, 0, -1, 1, 0, 0, -1, 0],
                [-1, 0, 1, -1, 1, 0, -1, 0, -1, 1],
                [-1, -1, 0, 0, 0, 0, 0, 0, 2, -1],
                [-1, 1, -1, 1, -1, 0, 1, 0, -1, 0],
            ],
            "同一列 swap({:?},{:?}) ... cols_state",
            p1,
            p2
        );

        assert_eq!(status.score, 92, "同一列 swap({:?},{:?}) ... score", p1, p2);
    }

    // 同一行
    {
        let mut field = get_problem();

        fill_blocks(&mut field);

        let mut status = Status::new(&field);

        // 同一行
        let p1 = Pos { row: 5, col: 3 }; // 4
        let p2 = Pos { row: 5, col: 4 }; // 2

        status.swap(&mut field, &p1, &p2);

        assert_eq!(
            field,
            [
                [5, 3, 7, 8, 7, 6, 9, 8, 7],
                [6, 4, 2, 1, 9, 5, 5, 4, 3],
                [1, 9, 8, 4, 3, 2, 2, 6, 1],
                [8, 9, 6, 9, 6, 7, 9, 8, 3],
                [4, 5, 3, 8, 5, 3, 7, 5, 1],
                [7, 2, 1, 2, 4, 1, 4, 2, 6],
                [9, 6, 8, 7, 6, 5, 2, 8, 6],
                [7, 5, 4, 4, 1, 9, 4, 3, 5],
                [3, 2, 1, 3, 8, 2, 1, 7, 9],
            ],
            "同一行 swap({:?},{:?}) ... field",
            p1,
            p2
        );

        assert_eq!(
            status.rows_state,
            [
                [-1, -1, -1, 0, -1, 0, 0, 2, 1, 0],
                [-1, 0, 0, 0, 1, 1, 0, -1, -1, 0],
                [-1, 1, 1, 0, 0, -1, 0, -1, 0, 0],
                [-1, -1, -1, 0, -1, -1, 1, 0, 1, 2],
                [-1, 0, -1, 1, 0, 2, -1, 0, 0, -1],
                [-1, 1, 2, -1, 1, -1, 0, 0, -1, -1],
                [-1, -1, 0, -1, -1, 0, 2, 0, 1, 0],
                [-1, 0, -1, 0, 2, 1, -1, 0, -1, 0],
                [-1, 1, 1, 1, -1, -1, -1, 0, 0, 0],
            ],
            "同一行 swap({:?},{:?}) ... rows_state",
            p1,
            p2
        );

        assert_eq!(
            status.cols_state,
            [
                [-1, 0, -1, 0, 0, 0, 0, 1, 0, 0],
                [-1, -1, 1, 0, 0, 1, 0, -1, -1, 1],
                [-1, 1, 0, 0, 0, -1, 0, 0, 1, -1],
                [-1, 0, 0, 0, 1, -1, -1, 0, 1, 0],
                [-1, 0, -1, 0, 0, 0, 1, 0, 0, 0],
                [-1, 0, 1, 0, -1, 1, 0, 0, -1, 0],
                [-1, 0, 1, -1, 1, 0, -1, 0, -1, 1],
                [-1, -1, 0, 0, 0, 0, 0, 0, 2, -1],
                [-1, 1, -1, 1, -1, 0, 1, 0, -1, 0],
            ],
            "同一行 swap({:?},{:?}) ... cols_state",
            p1,
            p2
        );

        assert_eq!(status.score, 92, "同一行 swap({:?},{:?}) ... score", p1, p2);
    }

    // 異行異列(同一ブロック内)
    {
        let mut field = get_problem();

        fill_blocks(&mut field);

        let mut status = Status::new(&field);

        // 行も列も異なる(同一ブロック内)
        let p1 = Pos { row: 7, col: 3 }; // 4
        let p2 = Pos { row: 6, col: 4 }; // 6

        status.swap(&mut field, &p1, &p2);

        assert_eq!(
            field,
            [
                [5, 3, 7, 8, 7, 6, 9, 8, 7],
                [6, 4, 2, 1, 9, 5, 5, 4, 3],
                [1, 9, 8, 4, 3, 2, 2, 6, 1],
                [8, 9, 6, 9, 6, 7, 9, 8, 3],
                [4, 5, 3, 8, 5, 3, 7, 5, 1],
                [7, 2, 1, 4, 2, 1, 4, 2, 6],
                [9, 6, 8, 7, 4, 5, 2, 8, 6],
                [7, 5, 4, 6, 1, 9, 4, 3, 5],
                [3, 2, 1, 3, 8, 2, 1, 7, 9],
            ],
            "異行異列 swap({:?},{:?}) ... field",
            p1,
            p2
        );

        assert_eq!(
            status.rows_state,
            [
                [-1, -1, -1, 0, -1, 0, 0, 2, 1, 0],
                [-1, 0, 0, 0, 1, 1, 0, -1, -1, 0],
                [-1, 1, 1, 0, 0, -1, 0, -1, 0, 0],
                [-1, -1, -1, 0, -1, -1, 1, 0, 1, 2],
                [-1, 0, -1, 1, 0, 2, -1, 0, 0, -1],
                [-1, 1, 2, -1, 1, -1, 0, 0, -1, -1],
                [-1, -1, 0, -1, 0, 0, 1, 0, 1, 0],
                [-1, 0, -1, 0, 1, 1, 0, 0, -1, 0],
                [-1, 1, 1, 1, -1, -1, -1, 0, 0, 0],
            ],
            "異行異列 swap({:?},{:?}) ... rows_state",
            p1,
            p2
        );

        assert_eq!(
            status.cols_state,
            [
                [-1, 0, -1, 0, 0, 0, 0, 1, 0, 0],
                [-1, -1, 1, 0, 0, 1, 0, -1, -1, 1],
                [-1, 1, 0, 0, 0, -1, 0, 0, 1, -1],
                [-1, 0, -1, 0, 1, -1, 0, 0, 1, 0],
                [-1, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                [-1, 0, 1, 0, -1, 1, 0, 0, -1, 0],
                [-1, 0, 1, -1, 1, 0, -1, 0, -1, 1],
                [-1, -1, 0, 0, 0, 0, 0, 0, 2, -1],
                [-1, 1, -1, 1, -1, 0, 1, 0, -1, 0],
            ],
            "異行異列 swap({:?},{:?}) ... cols_state",
            p1,
            p2
        );

        assert_eq!(
            status.score, 86,
            "異行異列 swap({:?},{:?}) ... score",
            p1, p2
        );
    }
}
